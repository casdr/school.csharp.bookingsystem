﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BookingSystem.Service.Contracts;
using BookingSystem.WebAPI.Models;

namespace BookingSystem.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/Type")]
    public class TypesController : ApiController
    {
        private ITypeService _typeService;
        public TypesController(ITypeService typeService)
        {
            _typeService = typeService;
        }

        [Route("GetTypes")]
        public IHttpActionResult GetTypes()
        {
            // Get all the types
            var types = new List<TypeViewModel>();
            var result = _typeService.GetTypes();
            result.ForEach(t =>
            {
                // Fill the type list
                var item = new TypeViewModel()
                {
                    Id = t.Id,
                    Name = t.Name
                };
                types.Add(item);
            });
            // Return all the types
            return Ok(types);
        }
    }
}