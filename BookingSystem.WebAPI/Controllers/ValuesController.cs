﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BookingSystem.DataAccess.UserManager;

namespace BookingSystem.WebAPI.Controllers
{
    [Authorize]
    public class ValuesController : ApiController
    {
        // Not used anymore. I can't remove it, visual code is giving me an error.
    }
}
